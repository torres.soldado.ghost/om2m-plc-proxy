package org.keeper.iot.wsn.plc.messages.sys;

import static com.esotericsoftware.minlog.Log.LEVEL_TRACE;

import com.esotericsoftware.minlog.Log;

import static org.junit.Assert.*;

import org.junit.BeforeClass;
import org.junit.Test;
import org.keeper.iot.wsn.plc.messages.Message;
import org.keeper.iot.wsn.plc.messages.MessageDebugHelpers;
import org.keeper.utils.Helpers;

import java.net.InetAddress;
import java.net.UnknownHostException;

public class SysGetSerialRequestTest {
  /**
   * @throws java.lang.Exception
   */
  @BeforeClass
  public static void setUpBeforeClass() throws Exception {
    Log.set(LEVEL_TRACE);
  }
  
  @Test
  public void test() {
    InetAddress address = null;
    try {
      address = InetAddress.getByName("192.168.100.1");
    } catch (UnknownHostException e) {
      e.printStackTrace();
      fail("Should not happen");
    }
    
    Message msg = SysGetSerialRequest.createMessage(address);
    
    byte[] data = {04, 00, (byte) 0xfe, 0x21, 0x04, (byte) 0xdb}; // by hand
    assertTrue(Helpers.compareArrays(msg.getRaw(), data));
    
    assertTrue(MessageDebugHelpers.isSameAddresses("test", address, msg.getAddress()));
  }

}
