/**
 * SysGetFirmwareVersionRequest.java
 * 
 * @author keeper
 */

package org.keeper.iot.wsn.plc.messages.sys;

import org.keeper.iot.wsn.plc.messages.Message;
import org.keeper.iot.wsn.plc.messages.MessageFields;

import java.net.InetAddress;

/**
 * Implements the message builder for SysGetFirmwareVersionRequest.
 * 
 * @author keeper
 * @version $Revision: 1.0 $
 */
public class SysGetFirmwareVersionRequest extends Message {
  /**
   * Used by logger.
   */
  private static final String LOG_TAG = SysGetFirmwareVersionRequest.class.getSimpleName();

  /**
   * Create message from raw data.
   * 
   * @param address Address of destination device.
   * 
   * @return Message.
   */
  public static Message createMessage(InetAddress address) {
    final byte[] messageData =
        {0, 0, MessageFields.SOF, MessageFields.CMDO_SREQ | MessageFields.SUBSYSTEM_SYS,
            Sys.SYS_CMD_GET_FW_VER, 0};
    Message message = new SysGetFirmwareVersionRequest(address, messageData);

    return message;
  }

  /**
   * SysGetFirmwareVersionRequest.
   * 
   * @param address Address of destination device.
   * @param data Raw message data.
   */
  private SysGetFirmwareVersionRequest(InetAddress address, byte[] data) {
    super(address, data, SysGetFirmwareVersionResponse.REGISTRATION_ID);
  }

  /**
   * Method toString.
   * 
   * @return String
   */
  public String toString() {
    return LOG_TAG + ": " + super.toString();
  }
}
