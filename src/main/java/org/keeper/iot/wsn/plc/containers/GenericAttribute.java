package org.keeper.iot.wsn.plc.containers;

public class GenericAttribute {
  /**
   * Field name.
   */
  public String name;
  /**
   * Field datatype.
   */
  public byte datatype;

  /**
   * Constructor for BasicAttribute.
   * 
   * @param name String
   * @param datatype byte
   */
  public GenericAttribute(String name, byte datatype) {
    this.name = name;
    this.datatype = datatype;
  }
}
