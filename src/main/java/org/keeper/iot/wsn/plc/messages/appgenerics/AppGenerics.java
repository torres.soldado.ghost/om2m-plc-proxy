/**
 * @author sergio.soldado@withus.pt
 */

package org.keeper.iot.wsn.plc.messages.appgenerics;

import java.net.InetAddress;
import java.util.List;

/**
 * Interface and attributes for the App subsystem Generics cluster.
 * 
 * @author sergio.soldado@withus.pt
 *
 */
public interface AppGenerics {
  /**
   * Cluster command value to get cluster version.
   */
  public static final byte GEN_CMD_GET_VERSION = (0);
  /**
   * Cluster command value to read attribute.
   */
  public static final byte GEN_CMD_READ = (1);
  /**
   * Cluster command value to write attribute.
   */
  public static final byte GEN_CMD_WRITE = (2);
  /**
   * Cluster command value to configure the periodic report for an attribute.
   */
  public static final byte GEN_CMD_CFG_PERIODIC_REPORT = (3);
  /**
   * Cluster command of a attribute report from the device.
   */
  public static final byte GEN_CMD_PERIODIC_REPORT = (4);
  /**
   * Cluster command value to configure the range alert for an attribute.
   */
  public static final byte GEN_CMD_CFG_RANGE_ALERT = (5);
  /**
   * Cluster command of an attribute range alert from the device.
   */
  public static final byte GEN_CMD_RANGE_ALERT = (6);
  /**
   * Cluster command value to get the endpoint's capabilities (clusters and attributes supported by
   * it).
   */
  public static final byte GEN_CMD_GET_CAPABILITIES = (7);

  /**
   * Response with cluster version if the status is successful.
   * 
   * @param address Address of device that originated this message.
   * @param endpoint Endpoint in device that originated this message.
   * @param status PLC status code bit-fields.
   * @param version Version of this cluster.
   * @param sequenceNumber Message sequence number.
   */
  public void onAppGenericsGetVersionResponse(InetAddress address, byte endpoint, int status,
      byte version, byte sequenceNumber);

  /**
   * Response with attribute read if the status is successful.
   * 
   * @param address Address of device that originated this message.
   * @param status PLC status code bit-fields.
   * @param endpoint Device endpoint that originated this message.
   * @param clusterId Cluster id.
   * @param attributeId Attribute id.
   * @param attributeId Attribute data type id.
   * @param attributeData Attribute data raw.
   * @param sequenceNumber Message sequence number.
   */
  public void onAppGenericsGetAttributeResponse(InetAddress address, int status, byte endpoint,
      byte clusterId, byte attributeId, byte attributeDatatype, byte[] attributeData, byte sequenceNumber);

  /**
   * Response of set attribute.
   * 
   * @param address Address of device that originated this message.
   * @param status PLC status code bit-fields.
   * @param sequenceNumber Message sequence number.
   */
  public void onAppGenericsSetAttributeResponse(InetAddress address, int status, byte clusterId,
      byte attributeId, byte sequenceNum);

  /**
   * Response of configure attribute periodic report.
   * 
   * @param address Address of device that originated this message.
   * @param status PLC status code bit-fields.
   * @param sequenceNumber Message sequence number.
   */
  public void onAppGenericsPeriodicReportConfigureResponse(InetAddress address, byte endpoint,
      int status, byte clusterId, byte attributeId, byte sequenceNum);

  /**
   * An attribute periodic report.
   * 
   * @param address Address of device that originated this message.
   * @param status PLC status code bit-fields.
   * @param endpoint Device endpoint that originated this message.
   * @param clusterId Cluster id.
   * @param attributeId Attribute id.
   * @param attributeData Attribute data raw.
   * @param sequenceNumber Message sequence number.
   */
  public void onAppGenericsPeriodicReport(InetAddress address, byte endpoint, int status,
      byte clusterId, byte attributeId, byte[] attributeData, byte sequenceNumber);

  /**
   * Response of configure attribute range alert.
   * 
   * @param address Address of device that originated this message.
   * @param status PLC status code bit-fields.
   * @param sequenceNumber Message sequence number.
   */
  public void onAppGenericsAlertConfigureResponse(InetAddress address, byte endpoint, int status,
      byte clusterId, byte attributeId, byte sequenceNum);

  /**
   * An attribute range alert
   * 
   * @param address Address of device that originated this message.
   * @param status PLC status code bit-fields.
   * @param endpoint Device endpoint that originated this message.
   * @param clusterId Cluster id.
   * @param attributeId Attribute id.
   * @param attributeData Attribute data raw.
   * @param alertType Type of alarm.
   * @param sequenceNumber Message sequence number.
   */
  public void onAppGenericsRangeAlert(InetAddress address, byte endpoint, int status,
      byte clusterId, byte attributeId, byte[] attributeData, byte alertType);

  class Capabilities {
    public byte version;
    public List<Byte> subsystems;
    public List<Endpoint> endpoints;

    @Override
    public String toString() {
      StringBuilder sb = new StringBuilder();

      sb.append("Version: " + Integer.toString(version) + "\n");

      sb.append("Subsystem ids supported:");
      for (Byte byt : subsystems) {
        sb.append(" " + Integer.toString(byt));
      }
      sb.append("\n");

      for (Endpoint endpoint : endpoints) {
        sb.append(endpoint.toString());
      }

      return sb.toString();
    }
  }

  class Endpoint {
    public byte id;
    public List<Cluster> clusters;

    @Override
    public String toString() {
      StringBuilder sb = new StringBuilder();

      sb.append("Endpoint(" + Integer.toString(id) + "):\n");

      for (Cluster cluster : clusters) {
        sb.append("\t" + cluster.toString() + "\n");
      }

      return sb.toString();
    }
  }

  class Cluster {
    public byte id;
    public List<Byte> attributes;

    @Override
    public String toString() {
      StringBuilder sb = new StringBuilder();

      sb.append("Cluster(" + Integer.toString(id) + "):");

      for (Byte byt : attributes) {
        sb.append(" attribute(" + Integer.toString(byt) + ")");
      }

      return sb.toString();
    }
  }

  /**
   * Capabilities response.
   * @param address Address of device that originated this message.
   * @param status PLC status code bit-fields.
   * @param capabilities The device's capabilities.
   */
  public void onAppGenericsCapabilities(InetAddress address, int status, Capabilities capabilities);
}
