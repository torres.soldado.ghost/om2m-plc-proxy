package org.keeper.iot.wsn.plc.messages.dummy;

import static com.esotericsoftware.minlog.Log.trace;

import org.keeper.iot.wsn.plc.messages.Message;
import org.keeper.iot.wsn.plc.messages.MessageFields;
import org.keeper.iot.wsn.plc.messages.nwk.Nwk;

import java.net.InetAddress;
import java.net.UnknownHostException;

public class DummyResponseMessage extends Message {
  /**
   * Used by logger.
   */
  private static final String LOG_TAG = DummyResponseMessage.class.getSimpleName();
  public static final int REGISTRATION_ID;
  public static final byte[] DATA;
  public static InetAddress dummyAddress = null;

  static {
    DATA =
        new byte[] {0, 0, MessageFields.SOF, MessageFields.CMDO_SRSP | MessageFields.SUBSYSTEM_NWK,
            Nwk.NWK_INVALID, 0};
    MessageFields.setLenAuto(DATA);
    MessageFields.setChecksumAuto(DATA);
    REGISTRATION_ID = MessageFields.createId(DATA);
    trace(LOG_TAG, "REGISTRATION_ID is " + Integer.toString(REGISTRATION_ID));
    try {
      dummyAddress = InetAddress.getByName("192.168.1.100");
    } catch (UnknownHostException e) {
      e.printStackTrace();
    }
  }

  public static Message create(InetAddress address) {
    Message message = new DummyResponseMessage(address, DATA);
    return message;
  }

  private DummyResponseMessage(InetAddress address, byte[] data) {
    super(address, data);
  }

  @Override
  public void callHandler(Object obs) {
    if (obs instanceof Dummy) {
      ((Dummy) obs).onDummyResponse(super.getAddress());
    }
  }

  public String toString() {
    return LOG_TAG + ": " + super.toString();
  }

}
