/**
 * Creates a modbus send data request {@link Message} from parameters.
 * 
 * @author sergio.soldado@withus.pt
 */

package org.keeper.iot.wsn.plc.messages.mb;

import org.keeper.iot.wsn.plc.messages.Message;
import org.keeper.iot.wsn.plc.messages.MessageFields;
import org.keeper.utils.Helpers;

import java.net.InetAddress;
import java.security.InvalidParameterException;

/**
 * @author keeper
 * @version $Revision: 1.0 $
 */
public class MbSendDataRequest extends Message {
  /**
   * Used by logger.
   */
  private static final String LOG_TAG = MbSendDataRequest.class.getSimpleName();

  /**
   * Create message from raw data.
   * 
   * @param address Address of destination device.
   * @param modbusData Modbus data to send.
   * 
   * @return Message.
   */
  public static Message createMessage(InetAddress address, byte[] modbusData) {
    if (modbusData.length > Mb.MB_MAX_MSG_LENGTH) {
      throw new InvalidParameterException("modbus data length to big");
    }

    final byte[] messageData = new byte[5 + modbusData.length + 1];
    MessageFields.setCmd0(messageData,
        (byte) (MessageFields.CMDO_SREQ | MessageFields.SUBSYSTEM_MB));
    MessageFields.setCmd1AkaClusterId(messageData, Mb.MB_COM_DATA);
    Helpers.arrayToArray(modbusData, messageData, 0, modbusData.length, 5);
    Message message = new MbSendDataRequest(address, messageData);

    return message;
  }

  /**
   * @param address Address of destination device.
   * @param data Raw message data.
   */
  private MbSendDataRequest(InetAddress address, byte[] data) {
    super(address, data, MbSendDataResponse.REGISTRATION_ID);
  }

  /*
   * (non-Javadoc)
   * 
   * @see org.keeper.iot.wsn.plc.messages.Message#toString()
   */
  /**
   * Method toString.
   * 
   * @return String
   */
  public String toString() {
    byte[] modbusData = new byte[getRaw().length - 6];
    Helpers.arrayToArray(getRaw(), modbusData, 5, modbusData.length, 0);
    return LOG_TAG + super.toString() + " ModbusData("
        + Helpers.arrayToHexStringPretty(modbusData, modbusData.length) + ")";
  }
}
