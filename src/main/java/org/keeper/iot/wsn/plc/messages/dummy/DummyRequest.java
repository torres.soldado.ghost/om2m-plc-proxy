package org.keeper.iot.wsn.plc.messages.dummy;

import org.keeper.iot.wsn.plc.messages.Message;
import org.keeper.iot.wsn.plc.messages.MessageFields;
import org.keeper.iot.wsn.plc.messages.nwk.Nwk;

import java.net.InetAddress;

public class DummyRequest extends Message {
  private static final String LOG_TAG = "DummyRequestMessage";


  /**
   * Create message from device address.
   * 
   * @param address Address of target device.
   * @return Message.
   */
  public static Message createMessage(InetAddress address) {
    byte[] messageData =
        new byte[] {0, 0, MessageFields.SOF, MessageFields.CMDO_SREQ | MessageFields.SUBSYSTEM_NWK,
            Nwk.NWK_INVALID, 0};
    Message message = new DummyRequest(address, messageData, DummyResponseMessage.REGISTRATION_ID);

    return message;
  }


  private DummyRequest(InetAddress address, byte[] data, int responseId) {
    super(address, data, responseId);
  }

  public String toString() {
    return LOG_TAG + ": " + super.toString();
  }
}
