/**
 * Creates a modbus receive data from device {@link Message} from raw data.
 * @author sergio.soldado@withus.pt
 */

package org.keeper.iot.wsn.plc.messages.mb;

import org.keeper.iot.wsn.plc.messages.Message;
import org.keeper.iot.wsn.plc.messages.MessageDebugHelpers;
import org.keeper.iot.wsn.plc.messages.MessageFields;
import org.keeper.iot.wsn.plc.messages.MessageStatus;
import org.keeper.utils.Helpers;

import java.net.InetAddress;

/**
 * @author keeper
 * @version $Revision: 1.0 $
 */
public class MbReceiveDataFromDevice extends Message {
  /**
   * Used by logger.
   */
  private static final String LOG_TAG = MbReceiveDataFromDevice.class.getSimpleName();
  /**
   * Used to register observer against this message Id.
   */
  public static final int REGISTRATION_ID = MessageFields.createId(new byte[] {0, 0,
      MessageFields.SOF, MessageFields.CMDO_AREQ | MessageFields.SUBSYSTEM_MB, Mb.MB_COM_DATA, 0});

  /**
   * The PLC status bit-fields.
   */
  private final int status;

  /**
   * Received modbus data.
   */
  private final byte[] modbusData;

  /**
   * Message parser.
   * 
   * @param address Address of device that originated this message.
   * @param data Raw message data from datagram.
   * 
   * @throws IllegalArgumentException if either parameter is null.
   */
  public MbReceiveDataFromDevice(InetAddress address, byte[] data)
      throws IllegalArgumentException {
    super(address, data);
    this.status = (int) Helpers.getUnsigned32FromArrayLittleEndian(data, 5);
    modbusData = new byte[data.length - 10];
    Helpers.arrayToArray(data, modbusData, 9, data.length - 10, 0);
  }

  /**
   * Don't allow.
   */
  private MbReceiveDataFromDevice() {
    super(null, null);
    this.status = 0;
    modbusData = null;
  }

  /**
   * Calls the correct handler, the object must implement the specific interface.
   * 
   * @param obs Object Observer on which the method will be called.
   * 
   * @see org.keeper.iot.wsn.plc.messages.Message#callHandler(java.lang.Object)
   */
  @Override
  public void callHandler(Object obs) {
    if (obs instanceof Mb) {
      ((Mb) obs).onMbReceiveDataRequest(super.getAddress(), this.status, modbusData);
    } else {
      MessageDebugHelpers.incompatibleHandler(LOG_TAG);
    }
  }

  /*
   * (non-Javadoc)
   * 
   * @see org.keeper.iot.wsn.plc.messages.Message#toString()
   */
  /**
   * Method toString.
   * 
   * @return String
   */
  @Override
  public String toString() {
    return LOG_TAG + ": " + super.toString() + ", Status("
        + MessageStatus.getStatusStr(this.status) + "), modbusData("
        + Helpers.arrayToHexStringPretty(modbusData, modbusData.length) + ")";
  }
}
