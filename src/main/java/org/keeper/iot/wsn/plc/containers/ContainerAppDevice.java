package org.keeper.iot.wsn.plc.containers;

import static com.esotericsoftware.minlog.Log.*;

import org.keeper.iot.wsn.plc.core.ICore;
import org.keeper.iot.wsn.plc.messages.Message;
import org.keeper.iot.wsn.plc.messages.MessageDebugHelpers;
import org.keeper.iot.wsn.plc.messages.MessageFields;
import org.keeper.iot.wsn.plc.messages.appdevice.*;
import org.keeper.iot.wsn.plc.messages.appgenerics.AppGenericsGetAttributeRequest;
import org.keeper.iot.wsn.plc.messages.appgenerics.AppGenericsGetAttributeResponse;
import org.keeper.iot.wsn.plc.messages.appgenerics.AppGenericsSetAttributeRequest;
import org.keeper.iot.wsn.plc.messages.appgenerics.AppGenericsSetAttributeResponse;
import org.keeper.iot.wsn.plc.observer.IObserver;

import java.net.InetAddress;
import java.util.List;

public class ContainerAppDevice implements IContainer, IObserver, AppDevice {

  /**
   * Used by logger.
   */
  private static final String TAG = ContainerAppDevice.class.getSimpleName();

  /**
   * Decorate {@link HandlerAppGenerics} just to get {@link AppDevice#DEV_ATT_CUSTOM_ID}.
   */
  private class AppGenericsLocalHandler extends ContainerAppGenerics {
    /**
     * @param parent The resource that contains this.
     */
    public AppGenericsLocalHandler(IContainer parent, String tag) {
      super(parent, tag);
    }

    @Override
    public void onAppGenericsGetAttributeResponse(InetAddress address, int status, byte endpoint,
        byte clusterId, byte attributeId, byte datatype, byte[] attributeData, byte sequenceNumber) {

      if (MessageDebugHelpers.isMessageForMeAndValidStatus(TAG,
          ContainerAppDevice.this.endpoint, endpoint, status)) {
        PlcResource resource = rootResource.getChild(endpoint, clusterId, attributeId);
        if (null != resource) {
          if (resource.getName().equalsIgnoreCase("ID")) {
            resource.setValue(new String(attributeData));
          }
          // else {
          // resource.setValue(attributeData);
          // }
        }
      }
    }

    @Override
    public void onAppGenericsSetAttributeResponse(InetAddress address, int status, byte clusterId,
        byte attributeId, byte sequenceNum) {
      PlcResource resource = rootResource.getChild(endpoint, clusterId, attributeId);
      
      if (MessageDebugHelpers.isMessageForMeAndValidStatus(TAG,
          ContainerAppDevice.this.endpoint, endpoint, status)) {
        trace(TAG, "Attribute remote write success for " + resource.getName());
      } else {
        error(TAG, "Attribute remote write error for " + resource.getName());
      }
    }


  }

  /**
   * The target device's address.
   */
  private final InetAddress address;
  /**
   * Reference to pre-initialised {@link Core}.
   */
  private final ICore core;
  /**
   * The target endpoint.
   */
  private final byte endpoint;

  private final PlcResource rootResource;
  private final PlcResource customIdResource;
  private final PlcResource versionResource;
  private final AppGenericsLocalHandler genericsHandler = new AppGenericsLocalHandler(this, TAG);

  public ContainerAppDevice(ICore core, InetAddress address, byte endpoint) {
    this.core = core;
    this.address = address;
    this.endpoint = endpoint;
    this.rootResource = new PlcResource("Device");

    versionResource = new PlcResource("Version") {
      @Override
      public void remoteGet() {
        core.send(AppDeviceGetVersionRequest.createMessage(address, endpoint));
      }
    };
    rootResource.addChild(versionResource);

    customIdResource =
        new PlcResource("ID", ContainerAppDevice.this.endpoint,
            MessageFields.APP_CLUSTER_DEVICE, AppDevice.DEV_ATT_CUSTOM_ID,
            AppDevice.DEV_ATT_CUSTOM_ID_DATATYPE, AppDevice.DEV_ATT_CUSTOM_ID_LENGTH) {
          @Override
          public void remoteGet() {
            core.send(AppGenericsGetAttributeRequest.createMessage(address, endpoint, clusterId,
                attributeId, datatype, datalen));
          }

          @Override
          public boolean remoteSet(List<String> parameters) {
            String customId = parameters.get(0);
            if (customId.length() != datalen) {
              error(TAG, "Wrong length for " + this.getName() + " of " + address.getHostAddress());
              return false;
            }
            core.send(AppGenericsSetAttributeRequest.createMessage(address, endpoint, clusterId,
                attributeId, datatype,
                ResourceHelpers.resourceStringToRaw(datatype, customId, datalen)));
            debug(TAG, "Sent write attribute for " + this.getName() + " w/value " + customId);
            return true;
          }
        };
    rootResource.addChild(customIdResource);

    this.core.subscribe(this, address, AppDeviceGetVersionResponse.getRegistrationId(endpoint));
    this.core.subscribe(genericsHandler, address,
        AppGenericsGetAttributeResponse.getRegistrationId(endpoint));
    this.core.subscribe(genericsHandler, address,
        AppGenericsSetAttributeResponse.getRegistrationId(endpoint));
  }

  @Override
  public void onAppDeviceGetVersionResponse(InetAddress address, byte endpoint, int status,
      byte version, byte sequenceNumber) {
    if (MessageDebugHelpers.isMessageForMeAndValidStatus(TAG, this.endpoint, endpoint, status)) {
      versionResource.setValue(String.valueOf(version));
    }
  }

  @Override
  public void onDeviceMessage(Message msg) {
    if (MessageDebugHelpers.isSameAddresses(TAG, this.address, msg.getAddress())) {
      msg.callHandler(this);
    }
  }

  @Override
  public void onDeviceMessageTimeout(int msgId) {
    MessageDebugHelpers.debugOnMessageTimeout(TAG, address, msgId);
  }

  @Override
  public InetAddress getAddress() {
    return address;
  }

  @Override
  public PlcResource getChild(String[] path) {
    return rootResource.getChild(path, 0);
  }

  @Override
  public PlcResource getRoot() {
    return rootResource;
  }

  @Override
  public void remoteSetAsync(String path, List<String> parameters) {
    MessageDebugHelpers.setResource(TAG, path, parameters, rootResource);
  }

  @Override
  public void remoteUpdateAll() {
    rootResource.remoteGetAll();
  }

}
