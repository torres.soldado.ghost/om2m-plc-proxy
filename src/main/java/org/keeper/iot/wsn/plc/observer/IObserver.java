package org.keeper.iot.wsn.plc.observer;

import org.keeper.iot.wsn.plc.messages.Message;

/**
 * Message observer interface.
 */
public interface IObserver {
  /**
   * Method onDeviceMessage.
   * @param msg Message
   */
  public void onDeviceMessage(Message msg);

  /**
   * Method onDeviceMessageTimeout.
   * @param msgId int
   */
  public void onDeviceMessageTimeout(int msgId);
}
