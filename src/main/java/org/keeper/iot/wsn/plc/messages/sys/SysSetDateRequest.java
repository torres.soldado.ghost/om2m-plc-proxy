/**
 * SysSetDateRequest.java
 * 
 * @author keeper
 */

package org.keeper.iot.wsn.plc.messages.sys;

import static com.esotericsoftware.minlog.Log.error;

import org.keeper.iot.wsn.plc.messages.Message;
import org.keeper.iot.wsn.plc.messages.MessageFields;
import org.keeper.utils.Helpers;

import java.net.InetAddress;
import java.security.InvalidParameterException;

/**
 * Implements the message builder for SysSetDateRequest.
 * 
 * @author keeper
 * @version $Revision: 1.0 $
 */
public class SysSetDateRequest extends Message {
  /**
   * Used by logger.
   */
  private static final String LOG_TAG = SysSetDateRequest.class.getSimpleName();

  /**
   * Create message from raw data.
   * 
   * @param address Address of destination device.
   * 
   * @return Message.
   */
  public static Message createMessage(InetAddress address, byte[] date) {
    if (date.length != SysAttributes.DATE_LEN) {
      error(LOG_TAG, "Bad date length");
      throw new InvalidParameterException("Bad date length");
    }
    final byte[] messageData =
        new byte[] {0, 0, MessageFields.SOF, MessageFields.CMDO_SREQ | MessageFields.SUBSYSTEM_SYS,
            Sys.SYS_CMD_SET_DATE, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    Helpers.arrayToArray(date, messageData, 0, SysAttributes.DATE_LEN, 5);
    Message message = new SysSetDateRequest(address, messageData);

    return message;
  }

  /**
   * SysSetDateRequest.
   * 
   * @param address Address of destination device.
   * @param data Raw message data.
   */
  private SysSetDateRequest(InetAddress address, byte[] data) {
    super(address, data, SysGetDateResponse.REGISTRATION_ID);
  }

  /**
   * Method toString.
   * 
   * @return String
   */
  public String toString() {
    byte[] date = new byte[] {0, 0, 0, 0, 0, 0, 0, 0};
    Helpers.arrayToArray(getRaw(), date, 5, SysAttributes.DATE_LEN, 0);
    return LOG_TAG + ": " + super.toString() + " date(" + Helpers.arrayToHexString(date, date.length)
        + ")";
  }
}
