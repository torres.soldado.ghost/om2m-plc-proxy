package org.keeper.iot.wsn.plc.messages;

public interface DataTypes {
  /**
   * Field DATATYPE_INT8_ID.
   */
  public static final byte DATATYPE_INT8_ID = 0x00;
  public static final byte DATATYPE_INT8_SIZE = 1;
  /**
   * Field DATATYPE_UINT8_ID.
   */
  public static final byte DATATYPE_UINT8_ID = 0x01;
  public static final byte DATATYPE_UINT8_SIZE = 1;
  /**
   * Datatype identifiers.
   */
  public static final byte DATATYPE_INT16_ID = 0x02;
  public static final byte DATATYPE_INT16_SIZE = 2;
  /**
   * Field DATATYPE_UINT16_ID.
   */
  public static final byte DATATYPE_UINT16_ID = 0x03;
  public static final byte DATATYPE_UINT16_SIZE = 2;
  /**
   * Field DATATYPE_INT32_ID.
   */
  public static final byte DATATYPE_INT32_ID = 0x04;
  public static final byte DATATYPE_INT32_SIZE = 4;
  /**
   * Field DATATYPE_UINT32_ID.
   */
  public static final byte DATATYPE_UINT32_ID = 0x05;
  public static final byte DATATYPE_UINT32_SIZE = 4;
  /**
   * Field DATATYPE_INT64_ID.
   */
  public static final byte DATATYPE_INT64_ID = 0x06;
  public static final byte DATATYPE_INT64_SIZE = 8;
  /**
   * Field DATATYPE_UINT64_ID.
   */
  public static final byte DATATYPE_UINT64_ID = 0x07;
  public static final byte DATATYPE_UINT64_SIZE = 8;
  /**
   * Field DATATYPE_FLOAT_ID.
   */
  public static final byte DATATYPE_FLOAT_ID = 0x08;
  public static final byte DATATYPE_FLOAT_SIZE = 4;
  /**
   * Field DATATYPE_DOUBLE_ID.
   */
  public static final byte DATATYPE_DOUBLE_ID = 0x09;
  public static final byte DATATYPE_DOUBLE_SIZE = 8;
  /**
   * Field DATATYPE_BYTE_ARRAY_ID.
   */
  public static final byte DATATYPE_BYTE_ARRAY_ID = 0x0a;
  /**
   * Field DATATYPE_INVALID.
   */
  public static final byte DATATYPE_INVALID = 0x0b;
  
  
  public byte[] dataTypeSize = {
      DATATYPE_INT8_SIZE,
      DATATYPE_UINT8_SIZE,
      DATATYPE_INT16_SIZE,
      DATATYPE_UINT16_SIZE,
      DATATYPE_INT32_SIZE,
      DATATYPE_UINT32_SIZE,
      DATATYPE_INT64_SIZE,
      DATATYPE_UINT64_SIZE,
      DATATYPE_FLOAT_SIZE,
      DATATYPE_DOUBLE_SIZE};
}
