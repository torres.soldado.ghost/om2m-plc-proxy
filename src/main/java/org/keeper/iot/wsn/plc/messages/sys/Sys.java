/**
 * @author sergio.soldado@withus.pt
 */

package org.keeper.iot.wsn.plc.messages.sys;

import java.net.InetAddress;

/**
 * Interface and attributes for the Sys subsystem.
 * 
 * @author sergio.soldado@withus.pt
 *
 */
public interface Sys {
  /**
   * Cmd1 value to get subsystem version.
   */
  public static final byte SYS_CMD_GET_VERSION = (0);
  /**
   * Cmd1 value to get manufacturer id.
   */
  public static final byte SYS_CMD_GET_MANUF_ID = (1);
  /**
   * Cmd1 value to get model.
   */
  public static final byte SYS_CMD_GET_MODEL = (2);
  /**
   * Cmd1 value to get hardware version.
   */
  public static final byte SYS_CMD_GET_HW_VER = (3);
  /**
   * Cmd1 value to get serial.
   */
  public static final byte SYS_CMD_GET_SERIAL = (4);
  /**
   * Cmd1 value to get firmware version.
   */
  public static final byte SYS_CMD_GET_FW_VER = (5);
  /**
   * Cmd1 value to get date.
   */
  public static final byte SYS_CMD_GET_DATE = (6);
  /**
   * Cmd1 value to set date.
   */
  public static final byte SYS_CMD_SET_DATE = (7);
  /**
   * Cmd1 value to issue firmware update.
   */
  public static final byte SYS_CMD_FW_UPDATE = (8);
  /**
   * Cmd1 value to issue reboot.
   */
  public static final byte SYS_CMD_REBOOT = (9);
  /**
   * Cmd1 value to get temperature.
   */
  public static final byte SYS_CMD_GET_TEMP = (10);
  /**
   * Cmd1 value to get power source.
   */
  public static final byte SYS_CMD_GET_PWRSRC = (11);
  /**
   * Cmd1 value to issue factory reset.
   */
  public static final byte SYS_CMD_RST_FACTORY = (12);
  /**
   * Cmd1 value to notify user action.
   */
  public static final byte SYS_CMD_LOCAL_ACTION = (13);
  /**
   * Cmd1 value to notify chunk upload.
   */
  public static final byte SYS_CMD_FW_UPLOAD_CHUNK = (14);

  /**
   * Response with Sys subsystem version if the status is successful.
   * 
   * @param address Address of device that originated this message.
   * @param status Plc status code bitfields.
   * @param version Version of this subsystem.
   */
  public void onSysGetVersionResponse(InetAddress address, int status, byte version);

  /**
   * Response with device's manufacturer id if the status is successful.
   * 
   * @param address Address of device that originated this message.
   * @param status Plc status code bitfields.
   * @param manufacturerId The manufacturer id, should be ascii.
   */
  public void onSysGetManufacturerIdResponse(InetAddress address, int status, byte[] manufacturerId);

  /**
   * Response with device's model id if the status is successful.
   * 
   * @param address Address of device that originated this message.
   * @param status Plc status code bitfields.
   * @param modelId The model id, should be ascii.
   */
  public void onSysGetModelIdResponse(InetAddress address, int status, byte[] modelId);

  /**
   * Response with device's hardware revision if the status is successful.
   * 
   * @param address Address of device that originated this message.
   * @param status Plc status code bitfields.
   * @param hwVersion Hardware revision e.g. 1.0.0.
   */
  public void onSysGetHardwareRevisionResponse(InetAddress address, int status, byte[] hwVersion);

  /**
   * Response with device's serial id if the status is successful.
   * 
   * @param address Address of device that originated this message.
   * @param status Plc status code bitfields.
   * @param serial Serial number, raw binary.
   */
  public void onSysGetSerialResponse(InetAddress address, int status, byte[] serial);

  /**
   * Response with device's firmware version if the status is successful.
   * 
   * @param address Address of device that originated this message.
   * @param status Plc status code bitfields.
   * @param fwVersion Firmware version e.g. 1.0.0.
   */
  public void onSysGetFirmwareVersionResponse(InetAddress address, int status, byte[] fwVersion);

  /**
   * Response with device's current date if the status is successful.
   * 
   * @param address Address of device that originated this message.
   * @param status Plc status code bitfields.
   * @param date Date of device (seconds since epoch).
   */
  public void onSysGetDateResponse(InetAddress address, int status, byte[] date);

  /**
   * Response to set date.
   * 
   * @param address Address of device that originated this message.
   * @param status Plc status code bitfields.
   */
  public void onSysSetDateResponse(InetAddress address, int status);

  /**
   * Response of instructing device to reboot and update it's firmware (which was previously
   * transfered).
   * 
   * @param address Address of device that originated this message.
   * @param status Plc status code bitfields.
   */
  public void onSysUpdateFirmwareResponse(InetAddress address, int status);

  /**
   * Response of instructing the device to perform reboot.
   * 
   * @param address Address of device that originated this message.
   * @param status Plc status code bitfields.
   */
  public void onSysRebootResponse(InetAddress address, int status);

  /**
   * Response with device's current temperature reading in celsius.
   * 
   * @param address Address of device that originated this message.
   * @param status Plc status code bitfields.
   * @param temperatureCelsius Temperature value in celsius.
   */
  public void onSysGetTemperature(InetAddress address, int status, byte temperatureCelsius);

  /**
   * Response with device's power sources bitfield.
   * 
   * @param address Address of device that originated this message.
   * @param status Plc status code bitfields.
   * @param powerSource Bitfields with power sources.
   */
  public void onSysGetPowerSource(InetAddress address, int status, byte powerSource);

  /**
   * Response of instructing device to perform factory reset.
   * 
   * @param address Address of device that originated this message.
   * @param status Plc status code bitfields.
   */
  public void onSysFactoryReset(InetAddress address, int status);

  /**
   * Device informing that user performed action (e.g. reboot, relay flip, etc.).
   * 
   * @param address Address of device that originated this message.
   * @param status Plc status code bitfields.
   * @param actions Bitfields of actions performed by user.
   */
  public void onSysUserDidSomething(InetAddress address, int status, int actions);

  /**
   * Response with firmware chunk upload result.
   * 
   * @param address Address of device that originated this message.
   * @param status Plc status code bitfields.
   */
  public void onSysFirmwareChunkUpload(InetAddress address, int status);
}
