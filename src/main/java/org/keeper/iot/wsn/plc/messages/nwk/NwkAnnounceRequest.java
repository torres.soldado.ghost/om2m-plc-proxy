/**
 * NwkAnnounceRequest.java
 * 
 * @author keeper
 */

package org.keeper.iot.wsn.plc.messages.nwk;

import org.keeper.iot.wsn.plc.messages.Message;
import org.keeper.iot.wsn.plc.messages.MessageFields;

import java.net.InetAddress;

/**
 * @author keeper Implements the message parser for NwkAnnounceRequest.
 * @version $Revision: 1.0 $
 */
public class NwkAnnounceRequest extends Message {
  /**
   * Used by logger.
   */
  private static final String LOG_TAG = NwkAnnounceRequest.class.getSimpleName();

  /**
   * Create NwkAnnounceRequest message.
   * 
   * @param address Address of destination device.
   * @param allowDeviceOnNetwork True if device is allowed on network, false otherwise.
   * @return Message.
   */
  public static Message createMessage(InetAddress address, boolean allowDeviceOnNetwork) {
    byte[] messageData =
        new byte[] {0, 0, MessageFields.SOF, MessageFields.CMDO_SREQ | MessageFields.SUBSYSTEM_NWK,
            Nwk.NWK_ANNOUNCE, 0, 0};
    setAllowDeviceOnNetwork(messageData, allowDeviceOnNetwork);
    Message message = new NwkAnnounceRequest(address, messageData);

    return message;
  }

  /**
   * Helper method to set allow message field.
   * 
   * @param data Raw message data.
   * @param allowDeviceOnNetwork True of device if allowed on network, false otherwise.
   */
  private static void setAllowDeviceOnNetwork(byte[] data, boolean allowDeviceOnNetwork) {
    data[5] = (byte) (allowDeviceOnNetwork ? 1 : 0);
  }

  /**
   * Message constructor. Don't allow direct usage.
   * 
   * @param address Address of destination device..
   * @param data Raw message data from datagram.
   */
  private NwkAnnounceRequest(InetAddress address, byte[] data) {
    super(address, data, NwkAnnounceResponse.REGISTRATION_ID);
  }

  /**
   * Check if device is allowed on network.
   * 
   * @return True of device if allowed on network, false otherwise.
   */
  private boolean isAllowDeviceOnNetwork() {
    return (getRaw()[4] == 1);
  }

  /*
   * (non-Javadoc)
   * 
   * @see org.keeper.iot.wsn.plc.messages.Message#toString()
   */
  public String toString() {
    return LOG_TAG + ": "
        + super.toString()
        + ", allowDeviceOnNetwork(" + isAllowDeviceOnNetwork() + ")";
  }
}
