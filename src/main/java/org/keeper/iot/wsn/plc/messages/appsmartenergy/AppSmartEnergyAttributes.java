package org.keeper.iot.wsn.plc.messages.appsmartenergy;

import org.keeper.iot.wsn.plc.messages.DataTypes;

public class AppSmartEnergyAttributes {
  // Consumed active energy A+)
  public static final byte SE_ATT_E_A_C__A = 0;
  // Produced active enegy A-)
  public static final byte SE_ATT_E_A_P__A = 1;
  // Reactive energy Q1 +Ri)
  public static final byte SE_ATT_E_R_P__Q1 = 2;
  // Reactive energy Q2 +Rc)
  public static final byte SE_ATT_E_R_P__Q2 = 3;
  // Reactive energy Q3 -Ri)
  public static final byte SE_ATT_E_R_N__Q3 = 4;
  // Reactive energy Q4 -Rc)
  public static final byte SE_ATT_E_R_N__Q4 = 5;
  // Consumed active energy A+) L1
  public static final byte SE_ATT_E_A_C__AL1 = 6;
  // Consumed active energy A+) L2
  public static final byte SE_ATT_E_A_C__AL2 = 7;
  // Consumed active energy A+) L3
  public static final byte SE_ATT_E_A_C__AL3 = 8;
  // Produced active energy A-) L1
  public static final byte SE_ATT_E_A_P__AL1 = 9;
  // Produced active energy A-) L2
  public static final byte SE_ATT_E_A_P__AL2 = 10;
  // Produced active energy A-) L3
  public static final byte SE_ATT_E_A_P__AL3 = 11;
  // Max demand active power + QI+QIV)
  public static final byte SE_ATT_P_A__Q1Q4MD = 12;
  // Max demand active power + QI+QIV) capture time)
  public static final byte SE_ATT_P_A__Q1Q4MDCT = 13;
  // Max demand active power + QII+QIII)
  public static final byte SE_ATT_P_A__Q2Q3MD = 14;
  // Max demand active power + QII+QIII) capture time)
  public static final byte SE_ATT_P_A__Q2Q3MDCT = 15;
  // Rate 1 contract 1 active energy +A)
  public static final byte SE_ATT_E_A_P__R1C1A = 16;
  // Rate 2 contract 1 active energy +A)
  public static final byte SE_ATT_E_A_P__R2C1A = 17;
  // Rate 3 contract 1 active energy +A)
  public static final byte SE_ATT_E_A_P__R3C1A = 18;
  // Rate 4 contract 1 active energy +A)
  public static final byte SE_ATT_E_A_P__R4C1A = 19;
  // Rate 5 contract 1 active energy +A)
  public static final byte SE_ATT_E_A_P__R5C1A = 20;
  // Rate 6 contract 1 active energy +A)
  public static final byte SE_ATT_E_A_P__R6C1A = 21;
  // Total Rate contract 1 active energy +A)
  public static final byte SE_ATT_E_A_P__TRC1A = 22;
  // Rate 1 contract 1 active energy -A)
  public static final byte SE_ATT_E_A_N__R1C1A = 23;
  // Rate 2 contract 1 active energy -A)
  public static final byte SE_ATT_E_A_N__R2C1A = 24;
  // Rate 3 contract 1 active energy -A)
  public static final byte SE_ATT_E_A_N__R3C1A = 25;
  // Rate 4 contract 1 active energy -A)
  public static final byte SE_ATT_E_A_N__R4C1A = 26;
  // Rate 5 contract 1 active energy -A)
  public static final byte SE_ATT_E_A_N__R5C1A = 27;
  // Rate 6 contract 1 active energy -A)
  public static final byte SE_ATT_E_A_N__R6C1A = 28;
  // Total Rate contract 1 active energy -A)
  public static final byte SE_ATT_E_A_N__TRC1A = 29;
  // Rate 1 contract 1 reactive energy QI +Ri)
  public static final byte SE_ATT_E_R_P__R1C1Q1 = 30;
  // Rate 2 contract 1 reactive energy QI +Ri)
  public static final byte SE_ATT_E_R_P__R2C1Q1 = 31;
  // Rate 3 contract 1 reactive energy QI +Ri)
  public static final byte SE_ATT_E_R_P__R3C1Q1 = 32;
  // Rate 4 contract 1 reactive energy QI +Ri)
  public static final byte SE_ATT_E_R_P__R4C1Q1 = 33;
  // Rate 5 contract 1 reactive energy QI +Ri)
  public static final byte SE_ATT_E_R_P__R5C1Q1 = 34;
  // Rate 6 contract 1 reactive energy QI +Ri)
  public static final byte SE_ATT_E_R_P__R6C1Q1 = 35;
  // Total Rate contract 1 reactive energy QI +Ri)
  public static final byte SE_ATT_E_R_P__TRC1Q1 = 36;
  // Rate 1 contract 1 reactive energy QII +Rc)
  public static final byte SE_ATT_E_R_P__R1C1Q2 = 37;
  // Rate 2 contract 1 reactive energy QII +Rc)
  public static final byte SE_ATT_E_R_P__R2C1Q2 = 38;
  // Rate 3 contract 1 reactive energy QII +Rc)
  public static final byte SE_ATT_E_R_P__R3C1Q2 = 39;
  // Rate 4 contract 1 reactive energy QII +Rc)
  public static final byte SE_ATT_E_R_P__R4C1Q2 = 40;
  // Rate 5 contract 1 reactive energy QII +Rc)
  public static final byte SE_ATT_E_R_P__R5C1Q2 = 41;
  // Rate 6 contract 1 reactive energy QII +Rc)
  public static final byte SE_ATT_E_R_P__R6C1Q2 = 42;
  // Total Rate contract 1 reactive energy QII +Rc)
  public static final byte SE_ATT_E_R_P__TRC1Q2 = 43;
  // Rate 1 contract 1 reactive energy QIII -Ri)
  public static final byte SE_ATT_E_R_N__R1C1Q3 = 44;
  // Rate 2 contract 1 reactive energy QIII -Ri)
  public static final byte SE_ATT_E_R_N__R2C1Q3 = 45;
  // Rate 3 contract 1 reactive energy QIII -Ri)
  public static final byte SE_ATT_E_R_N__R3C1Q3 = 46;
  // Rate 4 contract 1 reactive energy QIII -Ri)
  public static final byte SE_ATT_E_R_N__R4C1Q3 = 47;
  // Rate 5 contract 1 reactive energy QIII -Ri)
  public static final byte SE_ATT_E_R_N__R5C1Q3 = 48;
  // Rate 6 contract 1 reactive energy QIII -Ri)
  public static final byte SE_ATT_E_R_N__R6C1Q3 = 49;
  // Total Rate contract 1 reactive energy QIII -Ri)
  public static final byte SE_ATT_E_R_N__TRC1Q3 = 50;
  // Rate 1 contract 1 reactive energy QIV -Rc)
  public static final byte SE_ATT_E_R_N__R1C1Q4 = 51;
  // Rate 2 contract 1 reactive energy QIV -Rc)
  public static final byte SE_ATT_E_R_N__R2C1Q4 = 52;
  // Rate 3 contract 1 reactive energy QIV -Rc)
  public static final byte SE_ATT_E_R_N__R3C1Q4 = 53;
  // Rate 4 contract 1 reactive energy QIV -Rc)
  public static final byte SE_ATT_E_R_N__R4C1Q4 = 54;
  // Rate 5 contract 1 reactive energy QIV -Rc)
  public static final byte SE_ATT_E_R_N__R5C1Q4 = 55;
  // Rate 6 contract 1 reactive energy QIV -Rc)
  public static final byte SE_ATT_E_R_N__R6C1Q4 = 56;
  // Total Rate contract 1 reactive energy QIV -Rc)
  public static final byte SE_ATT_E_R_N__TRC1Q4 = 57;
  // Rate 1 contract 1 Maximum demand active power + last average)
  public static final byte SE_ATT_P_A_P__R1C1MDLA = 58;
  // Rate 1 contract 1 Maximum demand active power + capture time)
  public static final byte SE_ATT_P_A_P__R1C1MDCT = 59;
  // Rate 2 contract 1 Maximum demand active power + last average)
  public static final byte SE_ATT_P_A_P__R2C1MDLA = 60;
  // Rate 2 contract 1 Maximum demand active power + capture time)
  public static final byte SE_ATT_P_A_P__R2C1MDCT = 61;
  // Rate 3 contract 1 Maximum demand active power + last average)
  public static final byte SE_ATT_P_A_P__R3C1MDLA = 62;
  // Rate 3 contract 1 Maximum demand active power + capture time)
  public static final byte SE_ATT_P_A_P__R3C1MDCT = 63;
  // Rate 4 contract 1 Maximum demand active power + last average)
  public static final byte SE_ATT_P_A_P__R4C1MDLA = 64;
  // Rate 4 contract 1 Maximum demand active power + capture time)
  public static final byte SE_ATT_P_A_P__R4C1MDCT = 65;
  // Rate 5 contract 1 Maximum demand active power + last average)
  public static final byte SE_ATT_P_A_P__R5C1MDLA = 66;
  // Rate 5 contract 1 Maximum demand active power + capture time)
  public static final byte SE_ATT_P_A_P__R5C1MDCT = 67;
  // Rate 6 contract 1 Maximum demand active power + last average)
  public static final byte SE_ATT_P_A_P__R6C1MDLA = 68;
  // Rate 6 contract 1 Maximum demand active power + capture time)
  public static final byte SE_ATT_P_A_P__R6C1MDCT = 69;
  // Total Rate contract 1 Maximum demand active power + last average)
  public static final byte SE_ATT_P_A_P__TRC1MDLA = 70;
  // Total Rate contract 1 Maximum demand active power + capture time)
  public static final byte SE_ATT_P_A_P__TRC1MDCT = 71;
  // Rate 1 contract 1 Maximum demand active power - last average)
  public static final byte SE_ATT_P_A_N__R1C1MDLA = 72;
  // Rate 1 contract 1 Maximum demand active power - capture time)
  public static final byte SE_ATT_P_A_N__R1C1MDCT = 73;
  // Rate 2 contract 1 Maximum demand active power - last average)
  public static final byte SE_ATT_P_A_N__R2C1MDLA = 74;
  // Rate 2 contract 1 Maximum demand active power - capture time)
  public static final byte SE_ATT_P_A_N__R2C1MDCT = 75;
  // Rate 3 contract 1 Maximum demand active power - last average)
  public static final byte SE_ATT_P_A_N__R3C1MDLA = 76;
  // Rate 3 contract 1 Maximum demand active power - capture time)
  public static final byte SE_ATT_P_A_N__R3C1MDCT = 77;
  // Rate 4 contract 1 Maximum demand active power - last average)
  public static final byte SE_ATT_P_A_N__R4C1MDLA = 78;
  // Rate 4 contract 1 Maximum demand active power - capture time)
  public static final byte SE_ATT_P_A_N__R4C1MDCT = 79;
  // Rate 5 contract 1 Maximum demand active power - last average)
  public static final byte SE_ATT_P_A_N__R5C1MDLA = 80;
  // Rate 5 contract 1 Maximum demand active power - capture time)
  public static final byte SE_ATT_P_A_N__R5C1MDCT = 81;
  // Rate 6 contract 1 Maximum demand active power - last average)
  public static final byte SE_ATT_P_A_N__R6C1MDLA = 82;
  // Rate 6 contract 1 Maximum demand active power - capture time)
  public static final byte SE_ATT_P_A_N__R6C1MDCT = 83;
  // Total Rate contract 1 Maximum demand active power - last average)
  public static final byte SE_ATT_P_A_N__TRC1MDLA = 84;
  // Total Rate contract 1 Maximum demand active power - capture time)
  public static final byte SE_ATT_P_A_N__TRC1MDCT = 85;
  // Instantaneous Voltage L1
  public static final byte SE_ATT_V_I__L1 = 86;
  // Instantaneous Current L1
  public static final byte SE_ATT_C_I__L1 = 87;
  // Instantaneous Voltage L2
  public static final byte SE_ATT_V_I__L2 = 88;
  // Instantaneous Current L2
  public static final byte SE_ATT_C_I__L2 = 89;
  // Instantaneous Voltage L3
  public static final byte SE_ATT_V_I__L3 = 90;
  // Instantaneous Current L3
  public static final byte SE_ATT_C_I__L3 = 91;
  // Instantaneous Current Sum of all phases)
  public static final byte SE_ATT_C_I__ALL = 92;
  // Instantaneous Reactive power + L1
  public static final byte SE_ATT_P_R_P__Q1 = 93;
  // Instantaneous Reactive power + L1
  public static final byte SE_ATT_P_R_P__Q2 = 94;
  // Instantaneous Reactive power + L1
  public static final byte SE_ATT_P_R_N__Q3 = 95;
  // Instantaneous Reactive power + L1
  public static final byte SE_ATT_P_R_N__Q4 = 96;
  // Instantaneous Active power + L1
  public static final byte SE_ATT_P_A_P__L1 = 97;
  // Instantaneous Active power - L1
  public static final byte SE_ATT_P_A_N__L1 = 98;
  // Instantaneous Active power + L2
  public static final byte SE_ATT_P_A_P__L2 = 99;
  // Instantaneous Active power - L2
  public static final byte SE_ATT_P_A_N__L2 = 100;
  // Instantaneous Active power + L3
  public static final byte SE_ATT_P_A_P__L3 = 101;
  // Instantaneous Active power - L3
  public static final byte SE_ATT_P_A_N__L3 = 102;
  // Instantaneous Active power + Sum of all phases)
  public static final byte SE_ATT_P_A_P__ALL = 103;
  // Instantaneous Active power - Sum of all phases)
  public static final byte SE_ATT_P_A_N__ALL = 104;
  // Instantaneous Power factor
  public static final byte SE_ATT_P__PF = 105;
  // Instantaneous Power factor L1
  public static final byte SE_ATT_P__PFL1 = 106;
  // Instantaneous Power factor L2
  public static final byte SE_ATT_P__PFL2 = 107;
  // Instantaneous Power factor L3
  public static final byte SE_ATT_P__PFL3 = 108;
  // Instantaneous Frequency
  public static final byte SE_ATT_F = 109;
  // Meter Operation Mode
  public static final byte SE_OP_MODE = (byte) 200;
  
  public static final byte SE_OP_MODE_0 = 0;
  public static final byte SE_OP_MODE_1 = 1;
  public static final byte SE_OP_MODE_2 = 2;

  public static final byte SE_ATT_E_A_C__A_DATATYPE = DataTypes.DATATYPE_UINT64_ID;
  public static final byte SE_ATT_E_A_P__A_DATATYPE = DataTypes.DATATYPE_UINT64_ID;
  public static final byte SE_ATT_E_R_P__Q1_DATATYPE = DataTypes.DATATYPE_UINT64_ID;
  public static final byte SE_ATT_E_R_P__Q2_DATATYPE = DataTypes.DATATYPE_UINT64_ID;
  public static final byte SE_ATT_E_R_N__Q3_DATATYPE = DataTypes.DATATYPE_UINT64_ID;
  public static final byte SE_ATT_E_R_N__Q4_DATATYPE = DataTypes.DATATYPE_UINT64_ID;
  public static final byte SE_ATT_E_A_C__AL1_DATATYPE = DataTypes.DATATYPE_UINT64_ID;
  public static final byte SE_ATT_E_A_C__AL2_DATATYPE = DataTypes.DATATYPE_UINT64_ID;
  public static final byte SE_ATT_E_A_C__AL3_DATATYPE = DataTypes.DATATYPE_UINT64_ID;
  public static final byte SE_ATT_E_A_P__AL1_DATATYPE = DataTypes.DATATYPE_UINT64_ID;
  public static final byte SE_ATT_E_A_P__AL2_DATATYPE = DataTypes.DATATYPE_UINT64_ID;
  public static final byte SE_ATT_E_A_P__AL3_DATATYPE = DataTypes.DATATYPE_UINT64_ID;
  public static final byte SE_ATT_P_A__Q1Q4MD_DATATYPE = DataTypes.DATATYPE_UINT32_ID;
  public static final byte SE_ATT_P_A__Q1Q4MDCT_DATATYPE = DataTypes.DATATYPE_UINT32_ID;
  public static final byte SE_ATT_P_A__Q2Q3MD_DATATYPE = DataTypes.DATATYPE_UINT32_ID;
  public static final byte SE_ATT_P_A__Q2Q3MDCT_DATATYPE = DataTypes.DATATYPE_UINT32_ID;
  public static final byte SE_ATT_E_A_P__R1C1A_DATATYPE = DataTypes.DATATYPE_UINT64_ID;
  public static final byte SE_ATT_E_A_P__R2C1A_DATATYPE = DataTypes.DATATYPE_UINT64_ID;
  public static final byte SE_ATT_E_A_P__R3C1A_DATATYPE = DataTypes.DATATYPE_UINT64_ID;
  public static final byte SE_ATT_E_A_P__R4C1A_DATATYPE = DataTypes.DATATYPE_UINT64_ID;
  public static final byte SE_ATT_E_A_P__R5C1A_DATATYPE = DataTypes.DATATYPE_UINT64_ID;
  public static final byte SE_ATT_E_A_P__R6C1A_DATATYPE = DataTypes.DATATYPE_UINT64_ID;
  public static final byte SE_ATT_E_A_P__TRC1A_DATATYPE = DataTypes.DATATYPE_UINT64_ID;
  public static final byte SE_ATT_E_A_N__R1C1A_DATATYPE = DataTypes.DATATYPE_UINT64_ID;
  public static final byte SE_ATT_E_A_N__R2C1A_DATATYPE = DataTypes.DATATYPE_UINT64_ID;
  public static final byte SE_ATT_E_A_N__R3C1A_DATATYPE = DataTypes.DATATYPE_UINT64_ID;
  public static final byte SE_ATT_E_A_N__R4C1A_DATATYPE = DataTypes.DATATYPE_UINT64_ID;
  public static final byte SE_ATT_E_A_N__R5C1A_DATATYPE = DataTypes.DATATYPE_UINT64_ID;
  public static final byte SE_ATT_E_A_N__R6C1A_DATATYPE = DataTypes.DATATYPE_UINT64_ID;
  public static final byte SE_ATT_E_A_N__TRC1A_DATATYPE = DataTypes.DATATYPE_UINT64_ID;
  public static final byte SE_ATT_E_R_P__R1C1Q1_DATATYPE = DataTypes.DATATYPE_UINT64_ID;
  public static final byte SE_ATT_E_R_P__R2C1Q1_DATATYPE = DataTypes.DATATYPE_UINT64_ID;
  public static final byte SE_ATT_E_R_P__R3C1Q1_DATATYPE = DataTypes.DATATYPE_UINT64_ID;
  public static final byte SE_ATT_E_R_P__R4C1Q1_DATATYPE = DataTypes.DATATYPE_UINT64_ID;
  public static final byte SE_ATT_E_R_P__R5C1Q1_DATATYPE = DataTypes.DATATYPE_UINT64_ID;
  public static final byte SE_ATT_E_R_P__R6C1Q1_DATATYPE = DataTypes.DATATYPE_UINT64_ID;
  public static final byte SE_ATT_E_R_P__TRC1Q1_DATATYPE = DataTypes.DATATYPE_UINT64_ID;
  public static final byte SE_ATT_E_R_P__R1C1Q2_DATATYPE = DataTypes.DATATYPE_UINT64_ID;
  public static final byte SE_ATT_E_R_P__R2C1Q2_DATATYPE = DataTypes.DATATYPE_UINT64_ID;
  public static final byte SE_ATT_E_R_P__R3C1Q2_DATATYPE = DataTypes.DATATYPE_UINT64_ID;
  public static final byte SE_ATT_E_R_P__R4C1Q2_DATATYPE = DataTypes.DATATYPE_UINT64_ID;
  public static final byte SE_ATT_E_R_P__R5C1Q2_DATATYPE = DataTypes.DATATYPE_UINT64_ID;
  public static final byte SE_ATT_E_R_P__R6C1Q2_DATATYPE = DataTypes.DATATYPE_UINT64_ID;
  public static final byte SE_ATT_E_R_P__TRC1Q2_DATATYPE = DataTypes.DATATYPE_UINT64_ID;
  public static final byte SE_ATT_E_R_N__R1C1Q3_DATATYPE = DataTypes.DATATYPE_UINT64_ID;
  public static final byte SE_ATT_E_R_N__R2C1Q3_DATATYPE = DataTypes.DATATYPE_UINT64_ID;
  public static final byte SE_ATT_E_R_N__R3C1Q3_DATATYPE = DataTypes.DATATYPE_UINT64_ID;
  public static final byte SE_ATT_E_R_N__R4C1Q3_DATATYPE = DataTypes.DATATYPE_UINT64_ID;
  public static final byte SE_ATT_E_R_N__R5C1Q3_DATATYPE = DataTypes.DATATYPE_UINT64_ID;
  public static final byte SE_ATT_E_R_N__R6C1Q3_DATATYPE = DataTypes.DATATYPE_UINT64_ID;
  public static final byte SE_ATT_E_R_N__TRC1Q3_DATATYPE = DataTypes.DATATYPE_UINT64_ID;
  public static final byte SE_ATT_E_R_N__R1C1Q4_DATATYPE = DataTypes.DATATYPE_UINT64_ID;
  public static final byte SE_ATT_E_R_N__R2C1Q4_DATATYPE = DataTypes.DATATYPE_UINT64_ID;
  public static final byte SE_ATT_E_R_N__R3C1Q4_DATATYPE = DataTypes.DATATYPE_UINT64_ID;
  public static final byte SE_ATT_E_R_N__R4C1Q4_DATATYPE = DataTypes.DATATYPE_UINT64_ID;
  public static final byte SE_ATT_E_R_N__R5C1Q4_DATATYPE = DataTypes.DATATYPE_UINT64_ID;
  public static final byte SE_ATT_E_R_N__R6C1Q4_DATATYPE = DataTypes.DATATYPE_UINT64_ID;
  public static final byte SE_ATT_E_R_N__TRC1Q4_DATATYPE = DataTypes.DATATYPE_UINT64_ID;
  public static final byte SE_ATT_P_A_P__R1C1MDLA_DATATYPE = DataTypes.DATATYPE_UINT32_ID;
  public static final byte SE_ATT_P_A_P__R1C1MDCT_DATATYPE = DataTypes.DATATYPE_UINT32_ID;
  public static final byte SE_ATT_P_A_P__R2C1MDLA_DATATYPE = DataTypes.DATATYPE_UINT32_ID;
  public static final byte SE_ATT_P_A_P__R2C1MDCT_DATATYPE = DataTypes.DATATYPE_UINT32_ID;
  public static final byte SE_ATT_P_A_P__R3C1MDLA_DATATYPE = DataTypes.DATATYPE_UINT32_ID;
  public static final byte SE_ATT_P_A_P__R3C1MDCT_DATATYPE = DataTypes.DATATYPE_UINT32_ID;
  public static final byte SE_ATT_P_A_P__R4C1MDLA_DATATYPE = DataTypes.DATATYPE_UINT32_ID;
  public static final byte SE_ATT_P_A_P__R4C1MDCT_DATATYPE = DataTypes.DATATYPE_UINT32_ID;
  public static final byte SE_ATT_P_A_P__R5C1MDLA_DATATYPE = DataTypes.DATATYPE_UINT32_ID;
  public static final byte SE_ATT_P_A_P__R5C1MDCT_DATATYPE = DataTypes.DATATYPE_UINT32_ID;
  public static final byte SE_ATT_P_A_P__R6C1MDLA_DATATYPE = DataTypes.DATATYPE_UINT32_ID;
  public static final byte SE_ATT_P_A_P__R6C1MDCT_DATATYPE = DataTypes.DATATYPE_UINT32_ID;
  public static final byte SE_ATT_P_A_P__TRC1MDLA_DATATYPE = DataTypes.DATATYPE_UINT32_ID;
  public static final byte SE_ATT_P_A_P__TRC1MDCT_DATATYPE = DataTypes.DATATYPE_UINT32_ID;
  public static final byte SE_ATT_P_A_N__R1C1MDLA_DATATYPE = DataTypes.DATATYPE_UINT32_ID;
  public static final byte SE_ATT_P_A_N__R1C1MDCT_DATATYPE = DataTypes.DATATYPE_UINT32_ID;
  public static final byte SE_ATT_P_A_N__R2C1MDLA_DATATYPE = DataTypes.DATATYPE_UINT32_ID;
  public static final byte SE_ATT_P_A_N__R2C1MDCT_DATATYPE = DataTypes.DATATYPE_UINT32_ID;
  public static final byte SE_ATT_P_A_N__R3C1MDLA_DATATYPE = DataTypes.DATATYPE_UINT32_ID;
  public static final byte SE_ATT_P_A_N__R3C1MDCT_DATATYPE = DataTypes.DATATYPE_UINT32_ID;
  public static final byte SE_ATT_P_A_N__R4C1MDLA_DATATYPE = DataTypes.DATATYPE_UINT32_ID;
  public static final byte SE_ATT_P_A_N__R4C1MDCT_DATATYPE = DataTypes.DATATYPE_UINT32_ID;
  public static final byte SE_ATT_P_A_N__R5C1MDLA_DATATYPE = DataTypes.DATATYPE_UINT32_ID;
  public static final byte SE_ATT_P_A_N__R5C1MDCT_DATATYPE = DataTypes.DATATYPE_UINT32_ID;
  public static final byte SE_ATT_P_A_N__R6C1MDLA_DATATYPE = DataTypes.DATATYPE_UINT32_ID;
  public static final byte SE_ATT_P_A_N__R6C1MDCT_DATATYPE = DataTypes.DATATYPE_UINT32_ID;
  public static final byte SE_ATT_P_A_N__TRC1MDLA_DATATYPE = DataTypes.DATATYPE_UINT32_ID;
  public static final byte SE_ATT_P_A_N__TRC1MDCT_DATATYPE = DataTypes.DATATYPE_UINT32_ID;
  public static final byte SE_ATT_V_I__L1_DATATYPE = DataTypes.DATATYPE_UINT32_ID;
  public static final byte SE_ATT_C_I__L1_DATATYPE = DataTypes.DATATYPE_UINT32_ID;
  public static final byte SE_ATT_V_I__L2_DATATYPE = DataTypes.DATATYPE_UINT32_ID;
  public static final byte SE_ATT_C_I__L2_DATATYPE = DataTypes.DATATYPE_UINT32_ID;
  public static final byte SE_ATT_V_I__L3_DATATYPE = DataTypes.DATATYPE_UINT32_ID;
  public static final byte SE_ATT_C_I__L3_DATATYPE = DataTypes.DATATYPE_UINT32_ID;
  public static final byte SE_ATT_C_I__ALL_DATATYPE = DataTypes.DATATYPE_UINT32_ID;
  public static final byte SE_ATT_P_R_P__Q1_DATATYPE = DataTypes.DATATYPE_UINT32_ID;
  public static final byte SE_ATT_P_R_P__Q2_DATATYPE = DataTypes.DATATYPE_UINT32_ID;
  public static final byte SE_ATT_P_R_N__Q3_DATATYPE = DataTypes.DATATYPE_UINT32_ID;
  public static final byte SE_ATT_P_R_N__Q4_DATATYPE = DataTypes.DATATYPE_UINT32_ID;
  public static final byte SE_ATT_P_A_P__L1_DATATYPE = DataTypes.DATATYPE_UINT32_ID;
  public static final byte SE_ATT_P_A_N__L1_DATATYPE = DataTypes.DATATYPE_UINT32_ID;
  public static final byte SE_ATT_P_A_P__L2_DATATYPE = DataTypes.DATATYPE_UINT32_ID;
  public static final byte SE_ATT_P_A_N__L2_DATATYPE = DataTypes.DATATYPE_UINT32_ID;
  public static final byte SE_ATT_P_A_P__L3_DATATYPE = DataTypes.DATATYPE_UINT32_ID;
  public static final byte SE_ATT_P_A_N__L3_DATATYPE = DataTypes.DATATYPE_UINT32_ID;
  public static final byte SE_ATT_P_A_P__ALL_DATATYPE = DataTypes.DATATYPE_UINT32_ID;
  public static final byte SE_ATT_P_A_N__ALL_DATATYPE = DataTypes.DATATYPE_UINT32_ID;
  public static final byte SE_ATT_P__PF_DATATYPE = DataTypes.DATATYPE_UINT32_ID;
  public static final byte SE_ATT_P__PFL1_DATATYPE = DataTypes.DATATYPE_UINT32_ID;
  public static final byte SE_ATT_P__PFL2_DATATYPE = DataTypes.DATATYPE_UINT32_ID;
  public static final byte SE_ATT_P__PFL3_DATATYPE = DataTypes.DATATYPE_UINT32_ID;
  public static final byte SE_ATT_F_DATATYPE = DataTypes.DATATYPE_UINT32_ID;

}
