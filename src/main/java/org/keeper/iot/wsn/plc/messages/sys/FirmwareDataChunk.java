package org.keeper.iot.wsn.plc.messages.sys;

public class FirmwareDataChunk {
  public byte[] targetModel;
  public byte[] targetHardwareRevision;
  public byte[] targetFirmwareVersion;
  public byte[] chunkData;
  public int chunkOffset;
  public int chunkNumber;
  public int totalNumberOfChunks;
}
